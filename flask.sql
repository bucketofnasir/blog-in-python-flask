-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 13, 2017 at 08:19 AM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `flask`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `body` text NOT NULL,
  `author` varchar(100) NOT NULL,
  `posted_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `title`, `body`, `author`, `posted_on`) VALUES
(2, 'What is Lorem Ipsum?', 'asfasfsf', 'babor', '2017-12-13 05:52:02'),
(3, 'What is Lorem Ipsum?', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'babor', '2017-12-13 06:07:13'),
(4, 'Where can I get some?', '<p><span style=\"font-size:22px\"><span style=\"font-family:Comic Sans MS,cursive\"><strong><em>There are many variations of passages</em></strong></span></span></p>\r\n\r\n<ol>\r\n	<li>of Lorem</li>\r\n	<li>Ipsum available,</li>\r\n	<li>but the majority</li>\r\n</ol>\r\n\r\n<p>have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable.&nbsp;</p>\r\n', 'babor', '2017-12-13 06:29:46'),
(5, 'Java Object Oriented Programming Concepts', '<p>In this page, we will learn about basics of OOPs. Object Oriented Programming is a paradigm that provides many concepts such as&nbsp;<strong>inheritance</strong>,&nbsp;<strong>data binding</strong>,&nbsp;<strong>polymorphism</strong>&nbsp;etc.</p>\r\n\r\n<p><strong>Simula</strong>&nbsp;is considered as the first object-oriented programming language. The programming paradigm where everything is represented as an object, is known as truly object-oriented programming language.</p>\r\n\r\n<p><strong>Smalltalk</strong>&nbsp;is considered as the first truly object-oriented programming language.</p>\r\n\r\n<h2>OOPs (Object Oriented Programming System)</h2>\r\n\r\n<p><img alt=\"java oops concepts\" src=\"https://www.javatpoint.com/images/objects.jpg\" style=\"height:350px; width:350px\" /><strong>Object</strong>&nbsp;means a real word entity such as pen, chair, table etc.&nbsp;<strong>Object-Oriented Programming</strong>&nbsp;is a methodology or paradigm to design a program using classes and objects. It simplifies the software development and maintenance by providing some concepts:</p>\r\n\r\n<ul>\r\n	<li>Object</li>\r\n	<li>Class</li>\r\n	<li>Inheritance</li>\r\n	<li>Polymorphism</li>\r\n	<li>Abstraction</li>\r\n	<li>Encapsulation</li>\r\n</ul>\r\n\r\n<h2>Object</h2>\r\n\r\n<p>Any entity that has state and behavior is known as an object. For example: chair, pen, table, keyboard, bike etc. It can be physical and logical.</p>\r\n\r\n<h2>Class</h2>\r\n\r\n<p><strong>Collection of objects</strong>&nbsp;is called class. It is a logical entity.</p>\r\n\r\n<h4>Inheritance</h4>\r\n\r\n<p><strong>When one object acquires all the properties and behaviours of parent object</strong>&nbsp;i.e. known as inheritance. It provides code reusability. It is used to achieve runtime polymorphism.</p>\r\n\r\n<p><img alt=\"polymorphism in java oops concepts\" src=\"https://www.javatpoint.com/images/polymorphism.gif\" style=\"height:250px; width:250px\" /></p>\r\n\r\n<h4>Polymorphism</h4>\r\n\r\n<p>When&nbsp;<strong>one task is performed by different ways</strong>&nbsp;i.e. known as polymorphism. For example: to convince the customer differently, to draw something e.g. shape or rectangle etc.</p>\r\n\r\n<p>In java, we use method overloading and method overriding to achieve polymorphism.</p>\r\n\r\n<p>Another example can be to speak something e.g. cat speaks meaw, dog barks woof etc.</p>\r\n\r\n<h4>Abstraction</h4>\r\n\r\n<p><strong>Hiding internal details and showing functionality</strong>&nbsp;is known as abstraction. For example: phone call, we don&#39;t know the internal processing.</p>\r\n\r\n<p>In java, we use abstract class and interface to achieve abstraction.</p>\r\n\r\n<p><img alt=\"encapsulation in java oops concepts\" src=\"https://www.javatpoint.com/images/capsule.jpg\" style=\"height:100px; width:200px\" /></p>\r\n\r\n<h4>Encapsulation</h4>\r\n\r\n<p><strong>Binding (or wrapping) code and data together into a single unit is known as encapsulation</strong>. For example: capsule, it is wrapped with different medicines.</p>\r\n\r\n<p>A java class is the example of encapsulation. Java bean is the fully encapsulated class because all the data members are private here.</p>\r\n\r\n<hr />\r\n<h2>Advantage of OOPs over Procedure-oriented programming language</h2>\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>1)OOPs makes development and maintenance easier where as in Procedure-oriented programming language it is not easy to manage if code grows as project size grows.</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2)OOPs provides data hiding whereas in Procedure-oriented programming language a global data can be accessed from anywhere.</td>\r\n		</tr>\r\n		<tr>\r\n			<td>3)OOPs provides ability to simulate real-world event much more effectively. We can provide the solution of real word problem if we are using the Object-Oriented Programming language.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<td><img alt=\"Global Data\" src=\"https://www.javatpoint.com/images/globaldata2.JPG\" /></td>\r\n			<td><img alt=\"Object Data\" src=\"https://www.javatpoint.com/images/objectdata2.JPG\" /></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<hr />\r\n<h2>What is difference between object-oriented programming language and object-based programming language?</h2>\r\n\r\n<p>Object based programming language follows all the features of OOPs except Inheritance. JavaScript and VBScript are examples of object based programming languages.</p>\r\n', 'nasir', '2017-12-13 06:48:47');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `registration_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `registration_on`) VALUES
(3, 'Bangladesh', 'bangladesh', 'bangladesh@gmail.com', '$5$rounds=535000$LvRDByvTQxprrljR$3.L5hhvUoP./a6R07hnx7hF.5FiuLeCo.64zvjo9rF9', '2017-12-13 01:43:22'),
(4, 'Bangladesh', 'bangladesh', 'bangladesh@gmail.com', '$5$rounds=535000$isuiEH0rOwujGTKH$l4hj8h4mkuGR04SU.a2N66ZK3mmVNQ75EtdfB08uCd5', '2017-12-13 01:45:25'),
(5, 'Babor', 'babor', 'Babor@gmail.com', '$5$rounds=535000$uBQqq3AG5lUOGkkh$NZijn8fANF.0CFNuUlg0NwO2b4BQK6UyPOJFlweE0ZD', '2017-12-13 02:05:04'),
(7, 'Nasir Khan', 'nasir', 'nasir@gmail.com', '$5$rounds=535000$/gYM866r5Pf0vAVA$P89MYLbUR9CpkOfnGRDPcrnhIg9ZwoV6WN0pjCDKvU1', '2017-12-13 06:46:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
