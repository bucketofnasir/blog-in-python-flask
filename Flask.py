from flask import Flask, render_template, flash, redirect, session, url_for, request, logging
from flask_mysqldb import MySQL
from wtforms import Form, StringField, PasswordField, IntegerField, TextAreaField, validators
from passlib.hash import sha256_crypt
from functools import wraps

app = Flask(__name__)
app.config['MYSQL_HOST'] = '127.0.0.1'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'flask'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'
mysql = MySQL(app)



@app.route('/')
def index():
    return render_template('index.html')


@app.route('/blog')
def blog():
    cur = mysql.connection.cursor()
    r = cur.execute("SELECT * FROM article")
    art = cur.fetchall()
    if r:
        return render_template('blog.html', articles=art)
    else:
        msg="Sorry No article found"
        return render_template('blog.html', msg=msg)


@app.route('/article/<string:id>/')
def article(id):
    cur=mysql.connection.cursor()
    r=cur.execute("SELECT * FROM article WHERE id=%s",[id])
    art=cur.fetchone()
    if r:
        return render_template('article.html', article=art)
    else:
        msg="Sorry No article found"
        return render_template('article.html', msg=msg)


class RegistrationForm(Form):
    name = StringField('Name', [validators.Length(min=1, max=50)])
    username = StringField('Username', [validators.Length(min=4, max=25)])
    email = StringField('Email', [validators.Length(min=6, max=50)])
    password = PasswordField('Password', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message='Passwords do not match')
    ])
    confirm = PasswordField('Confirm Password')


@app.route('/register', methods=['GET', 'POST'])
def register():
    if 'logged_in' in session:
        flash('Logging out first', 'success')
        return redirect(url_for('dashboard'))
    form = RegistrationForm(request.form)
    if request.method == 'POST' and form.validate():
        name = form.name.data
        username = form.username.data
        email = form.email.data
        password = form.password.data
        password = sha256_crypt.encrypt(str(password))
        # Create cursor
        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO users(name, username, email, password) VALUES(%s,%s,%s,%s)",
                    (name, username, email, password))
        # commit to db
        mysql.connection.commit()
        cur.close()

        flash('You are now registered and can login', 'success')
        return redirect(url_for('login'))
    return render_template('register.html', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if 'logged_in' in session:
        flash('You are already logged in', 'success')
        return redirect(url_for('dashboard'))
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        cur = mysql.connection.cursor()
        result = cur.execute("SELECT * FROM users WHERE username=%s", [username])
        if result:
            data = cur.fetchone()
            p = data['password']

            if sha256_crypt.verify(password, p):
                session['logged_in'] = True
                session['username'] = data['username']
                flash('You are now logged in', 'success')
                return redirect(url_for('dashboard'))
            else:
                error = 'Error password'
                return render_template('login.html', error=error)
        else:
            error = 'Username is not matched'
            return render_template('login.html', error=error)

    return render_template('login.html')


def is_logged_in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash('Unauthorized, Please login', 'danger')
            return redirect(url_for('login'))

    return wrap


@app.route('/dashboard')
@is_logged_in
def dashboard():
    cur = mysql.connection.cursor()
    user=str(session['username'])
    result = cur.execute("SELECT * FROM article WHERE author=%s",[user])
    data = cur.fetchall()
    if result:
        return render_template('dashboard.html', data=data)
    else:
        msg="No data found"
        return render_template('dashboard.html', msg=msg)


class CreateArticle(Form):
    title = StringField('Title', [validators.Length(min=20, max=250)])
    body = TextAreaField('Body', [validators.Length(min=50)])


@app.route('/create', methods=['GET', 'POST'])
@is_logged_in
def createArticle():
    form = CreateArticle(request.form)
    if request.method == 'POST' and form.validate():
        title = form.title.data
        body = form.body.data
        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO article(title, body, author) VALUES(%s, %s, %s)", (title, body, session['username']))
        mysql.connection.commit()
        cur.close()
        flash('Post is published', 'success')
        return redirect(url_for('createArticle'))
    return render_template('create.html', form=form)


@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))


from Flask import

if __name__ == '__main__':
    app.secret_key = 'secret123'
    app.run(debug=True)
